// Create a class for a book that represents book’s
//  information such as title, author, and publication year. 
// The class should provide methods for retrieving and 
// updating the book’s details. Additionally, implement a 
// separate class for library that manages a collection of 
// books, can be an array of books as well. The library 
// class should have methods for adding books, searching 
// for books by title or author, and displaying the total 
// number of books in the library.

function Book(){
    let bookTitle, bookAuthor, bookPublicationYear;
    function constructor(bookTitle, bookAuthor, bookPublicationYear){
        this.bookTitle = bookTitle;
        this.bookAuthor = bookAuthor;
        this.bookPublicationYear = bookPublicationYear;
    }
    
    this.getBookDetails = function(){
        const bookDetails = {
            title : this.bookTitle,
            author : this.bookAuthor,
            publication : this.bookPublicationYear
        } 
    }
    this.updateBookDetails = function(bookTitle, bookAuthor, bookPublicationYear){
        if(bookTitle && bookTitle.length === 0){
            return 'Book Title cannot be empty';
        }else{
            this.bookTitle = bookTitle;
        }

        if(bookAuthor && bookAuthor.length === 0){
            return 'Book Author cannot be empty';
        }else{
            this.bookAuthor = bookAuthor;
        }

        if(bookPublicationYear && typeof(bookPublicationYear) !== 'number'){
            return 'Book PublicationYear should be a number';
        }
        else{
            this.bookPublicationYear = bookPublicationYear;
        }
    }
}

const bookOne = new Book('The Train','Komal', '1996');

function BookLibray(){
    let booksList = [];
    function constructor(bookList){
        this.bookList = bookList;
    }
    
    this.addBooks = function(book){
        this.bookList.push(book);
    }

    this.getBooksList = function(){
       return this.bookList;
    }

    this.searchBookss = function(searchValue, serachByvalue){
        if(searchValue && bookTitle.length === 0){
            return this.booksList;
        }else{
            const filteredList = this.bookList.filter((book)=> book[serachByvalue]=== searchValue);
            return filteredList;
        }
    }
}
